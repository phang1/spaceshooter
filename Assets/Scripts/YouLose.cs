﻿using UnityEngine;
using System.Collections;

public class YouLose : MonoBehaviour {
	
	void Start(){
		var guiComponent = GetComponent("GUIText").GetComponent<GUIText>();
		guiComponent.enabled = false;
	}
	
	public void turnOn(){
		var guiComponent = GetComponent("GUIText").GetComponent<GUIText>();
		guiComponent.enabled = true;
	}
	
}
