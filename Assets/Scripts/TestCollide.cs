﻿using UnityEngine;
using System.Collections;

public class TestCollide : MonoBehaviour {
	public GameObject spawner;
	public SpawnEnemy trackScore;

	void Start()
	{
		spawner = GameObject.FindWithTag ("Respawn");
		if(spawner != null){
			trackScore = spawner.GetComponent<SpawnEnemy>();
		}
	}

	//if there is a collision detection
	void OnCollisionEnter2D(Collision2D onHit){
	
		//if the enemy is hit
		if (onHit.gameObject.tag == "TestEnemy") {
						//destroy both this rocket, and the enemy
						Destroy (onHit.gameObject);
						Destroy (this.gameObject);
						trackScore.increaseScore ();
				} else if (onHit.gameObject.tag == "Player") {
			trackScore.decreaseScore();
			trackScore.decreaseScore();
			Destroy(this.gameObject);
				} else
			Destroy (this.gameObject);
		}
}
