﻿using UnityEngine;
using System.Collections;

public class testEnemy1 : MonoBehaviour {
	//declare a count to control the frequency of enemy direction
	public int count = 0;
	public Rigidbody2D thisShip;
	
	//declare and set the ship speed
	public float shipSpeed = .5f;

	SpawnEnemy trackScore;
	GameObject Spawn;

	// Use this for initialization
	void Start () {
		//set the trackScore to the spawn instance
		Spawn = GameObject.FindWithTag ("Respawn");
		if(Spawn != null){
			trackScore = Spawn.GetComponent<SpawnEnemy>();
		}
		//automatically propel the gameobject forward upon spawning
		GetComponent<Rigidbody2D>().velocity = new Vector2 (0, -shipSpeed);
	}
	
	// Update is called once per frame
	void Update () {
		//increment the count
		count++;
		//every 40 frames, change direction or stay the same direction
		if (count == 40) {
			int i = Random.Range (0, 3);
			if (i == 0)
				thisShip.velocity = new Vector2 (0, -shipSpeed);
			else if (i == 1)
				thisShip.velocity = new Vector2 (shipSpeed, -shipSpeed);
			else 
				thisShip.velocity = new Vector2 (-shipSpeed, -shipSpeed);
			//reset the count
			count = 0;

		}	

		
	}
	
	void OnCollisionEnter2D(Collision2D hit)
	{	
		//if there is a collision of this gameobject and the player gameobject
		if (hit.gameObject.tag == "Player") {
						//destroy this gameobject
			Destroy (this.gameObject);
				//decrease the score by 100 if the player is hit
			trackScore.decreaseScore();
			trackScore.decreaseScore();
		} else if (hit.gameObject.tag == "Backwall") {
			//decrease the score by 50 if the back wall is hit
			trackScore.decreaseScore();		
			Destroy(this.gameObject);
				}
	}
}
