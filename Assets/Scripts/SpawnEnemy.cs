﻿using UnityEngine;
using System.Collections;

public class SpawnEnemy : MonoBehaviour {

	//have two different general enemy spawns
	public Rigidbody2D EnemyOne;
	public Rigidbody2D EnemyTwo;
	public Rigidbody2D EnemyThree;
	private int timeCounter;

	public int playerScore;

	//used to ensure only three enemies are spawning
	GameObject[] trackEnemy;
	GameObject PlayerObject;
	Player player;

	public AudioClip background;

	// Use this for initialization
	void Start () {
		//begin with one enemy already spawned
		SpawnOne ();
		playerScore = 0;
		timeCounter = 0;

		//sets the player to the Player instance
		PlayerObject = GameObject.FindWithTag ("Player");
		player = PlayerObject.GetComponent<Player> ();

		GetComponent<AudioSource>().clip = background;
		GetComponent<AudioSource>().Play ();

	}
	
	// Update is called once per frame
	void Update () {

		trackEnemy = GameObject.FindGameObjectsWithTag ("TestEnemy");

		//every second, spawn an enemy if there is less than 3 ships instantiated
		if (timeCounter == 60) {
			if(trackEnemy.Length < 3){

				int i = Random.Range (0, 3);
				if (i == 0)
					SpawnOne();
				else if (i == 1)
					SpawnTwo();
				else if (i == 2)
					SpawnThree();
			}

			timeCounter = 0;
		}

		timeCounter++;
	}

	void OnGUI () {
		// Make a background box to display score
		GUI.Box(new Rect(10,10,100,90), "Score");
		GUI.Label (new Rect (60, 45, 100, 90), playerScore.ToString());

		//makea background boc to display life
		GUI.Box (new Rect (110, 10, 50, 50), "Lives");
		GUI.Label (new Rect(130, 30, 50, 50), player.playerLife.ToString());
	}

	//spawn the prefab of enemy one
	public void SpawnOne(){
		Rigidbody2D newEnemy = Instantiate (EnemyOne, transform.position, transform.rotation) as Rigidbody2D;
	}

	//spawn the prefab of enemy two
	public void SpawnTwo(){
		Rigidbody2D newEnemy = Instantiate (EnemyTwo, transform.position, transform.rotation) as Rigidbody2D;
	}

	public void SpawnThree(){
		Rigidbody2D newEnemy = Instantiate (EnemyThree, transform.position, transform.rotation) as Rigidbody2D;
	}

	//increase the score
	public void increaseScore(){
				playerScore += 100;
		}

	//decrease the score
	public void decreaseScore(){
		if(playerScore>0)
				playerScore -= 50;
		}

	public void turnOff(){
		for (int i = 0; i<trackEnemy.Length; i++) {
			Destroy(trackEnemy[i]);
				}
		this.enabled = false;
	}
}
