﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour {
	//declare a generic rigidbody
	public Rigidbody2D rocket;
	//declare and set the speed
	public float speed = .1f;
	//declare timeCount to keep track of the time
	public int timeCount = 0;

	public AudioClip shot;

	// Update is called once per frame
	void Update () {
		//if the jump button is pressed, and 60 frames or more have been rendered
		if (Input.GetButtonDown ("Jump") && timeCount>60) {
			//then fire a rocket and reset the timeCount
			FireRocket ();
			timeCount=0;

			GetComponent<AudioSource>().clip = shot;
			GetComponent<AudioSource>().Play ();
		}

		//increment the passing frames
		timeCount++;
	}

	void FireRocket(){
				//instatiate a clone of the rocket rigidbody that was earlier declared
				Rigidbody2D rocketShoot = (Rigidbody2D)Instantiate (rocket, transform.position, Quaternion.Euler (new Vector3 (0, 0, 0)));
				//set the velocity of this rigidbody forward from the position of the parent object(the player)
				rocketShoot.velocity = new Vector2 (0, speed);
		}
}
