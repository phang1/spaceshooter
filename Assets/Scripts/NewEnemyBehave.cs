﻿using UnityEngine;
using System.Collections;

public class NewEnemyBehave : MonoBehaviour {
	//declare a count to control the frequency of enemy direction
	public int count = 0;
	
	//declare and set the ship speed
	public float shipSpeed = .5f;
	
//	public Gui keepScore;
//	public SpawnEnemy newSpawn;
	
	void Awake(){
		//newSpawn.respawn = 1;
//		keepScore = GetComponent<Gui>();
//		newSpawn.SpawnOne ();
	}
	
	// Use this for initialization
	void Start () {
		//automatically propel the gameobject forward upon spawning
		GetComponent<Rigidbody2D>().velocity = new Vector2 (0, -shipSpeed);
	}
	
	// Update is called once per frame
	void Update () {
		//increment the count
		count++;
		//every 40 frames, change direction or stay the same direction
		if (count == 40) {
			int i = Random.Range (0, 3);
			if (i == 0)
				GetComponent<Rigidbody2D>().velocity = new Vector2 (0, -shipSpeed);
			else if (i == 1)
				GetComponent<Rigidbody2D>().velocity = new Vector2 (shipSpeed, -shipSpeed);
			else 
				GetComponent<Rigidbody2D>().velocity = new Vector2 (-shipSpeed, -shipSpeed);
			//reset the count
			count = 0;
		}	
		
	}
	
	
	
	void OnCollisionEnter2D(Collision2D hit)
	{		//if there is a collision of this gameobject and the player gameobject
		if (hit.gameObject.tag == "Player" || hit.gameObject.tag == "Background") {
			//destroy this gameobject
			Destroy (this.gameObject);
//			newSpawn.decreaseSpawn ();
		} 
		else if (hit.gameObject.tag == "Rocket") {	
//			keepScore.increaseScore();
		}
		
		
	}
}
