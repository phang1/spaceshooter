﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public int playerLife = 2;
	public Rocket rocketShoot;

	GameObject spawnScriptObject;
	SpawnEnemy spawnScript;

	GameObject youWinObject;
	YouWin youWin;

	GameObject youLoseObject;
	YouLose youLose;

	// Use this for initialization
	void Start () {
		spawnScriptObject = GameObject.FindWithTag ("Respawn");
		spawnScript = spawnScriptObject.gameObject.GetComponent<SpawnEnemy> ();

		youWinObject = GameObject.FindWithTag ("Finish");
		youWin = youWinObject.gameObject.GetComponent<YouWin> ();

		youLoseObject = GameObject.FindWithTag ("YouLose");
		youLose = youLoseObject.gameObject.GetComponent<YouLose> ();
	}
	
	// Update is called once per frame
	void Update () {
		//declare and set speed, 
		var shipSpeed = 1f;
		//declare variable for horizontal movement only, to restrict vertical movement
		var horMove = Input.GetAxis ("Horizontal");

		//set the velocity of the game object to the directional buttons
		GetComponent<Rigidbody2D>().velocity = new Vector2 (horMove * shipSpeed, 0);

		if (spawnScript.playerScore >= 2500) {
			spawnScript.turnOff();
			youWin.turnOn();
				}
	}




	void OnCollisionEnter2D(Collision2D hit)
	{
		//if the player is hit 2 times, then the player is dead
		if (hit.gameObject.tag == "TestEnemy") {
			playerLife--;
				if(playerLife == 0){
				spawnScript.turnOff();
				youLose.turnOn();
				Destroy(this.gameObject);
				}
		}
	}

}
